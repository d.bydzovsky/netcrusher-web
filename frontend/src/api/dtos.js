const createDefaultThrottlingConfig = () => {
    return {
        value: 0,
        timeUnit: "SECONDS",
        mode: "OFF"
    }
};
export const createNewProxyDto = (conn) =>
    JSON.stringify({
        connection: conn,
        downloadThrottling: createDefaultThrottlingConfig(),
        uploadThrottling: createDefaultThrottlingConfig(),
        state: "OPEN",
        persistCommunication: true
    });
export const createBulkActionDto = (action) => JSON.stringify({bulkAction: action});
export const createGranularProxyUpdateDto = (action, partialUpdate) =>
    JSON.stringify({
        action: action,
        partialUpdate: partialUpdate
    });