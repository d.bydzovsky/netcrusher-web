package com.horcacorp.testing.netcrusherweb.persistance.api

import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyHistory

interface ProxyHistoryPersistor {
    suspend fun getHistory(bindPort: Int): List<ProxyHistory>
    suspend fun saveHistory(bindPort: Int, item: ProxyHistory)
    suspend fun moveHistory(oldBindPort: Int, newBindPort: Int)
    suspend fun removeHistory(bindPort: Int)
}