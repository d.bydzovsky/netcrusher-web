package com.horcacorp.testing.netcrusherweb.persistance.impl

import com.horcacorp.testing.netcrusherweb.configuration.ConfigurationProvider
import com.horcacorp.testing.netcrusherweb.persistance.api.PersistFilenameFactory
import java.io.File

class PersistFilenameFactoryImpl(config: ConfigurationProvider) : PersistFilenameFactory {
    private val CONFIG_DIR = "config"
    private val TRAFFIC_DIR = "traffic"
    private val HISTORY_DIR = "history"

    private val PATH = if (config.devEnv) "" else "..${File.separator}"

    override fun getConfigFile() = File(File("$PATH$CONFIG_DIR").also { it.mkdirs() }, "config.json")

    override fun getUploadTrafficFile(bindPort: Int) =
        File(File("$PATH$TRAFFIC_DIR").also { it.mkdirs() }, "${bindPort}_up.txt")

    override fun getDownloadTrafficFile(bindPort: Int) =
        File(File("$PATH$TRAFFIC_DIR").also { it.mkdirs() }, "${bindPort}_down.txt")

    override fun getHistoryFile(bindPort: Int) =
        File(File("$PATH$HISTORY_DIR").also { it.mkdirs() }, "${bindPort}_history.csv")
}