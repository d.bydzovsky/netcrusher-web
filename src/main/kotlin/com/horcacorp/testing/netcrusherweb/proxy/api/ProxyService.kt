package com.horcacorp.testing.netcrusherweb.proxy.api

interface ProxyService {

    suspend fun add(proxyModel: ProxyModel)
    suspend fun open(bindPort: Int)
    suspend fun openAll()
    suspend fun close(bindPort: Int)
    suspend fun closeAll()
    suspend fun freeze(bindPort: Int)
    suspend fun freezeAll()
    suspend fun remove(bindPort: Int)
    suspend fun removeAll()
    suspend fun editDescription(bindPort: Int, newDescription: String)
    suspend fun editConnection(bindPort: Int, newConnection: ProxyConnection)
    suspend fun editThrottling(bindPort: Int, newDownConfig: ThrottlingConfig, newUpConfig: ThrottlingConfig)
    suspend fun turnOnCommunicationPersisting(bindPort: Int)
    suspend fun turnOffCommunicationPersisting(bindPort: Int)
    suspend fun getAll(): List<ProxyModel>
    suspend fun getHistory(bindPort: Int): List<ProxyHistory>
    suspend fun getAllWithHistory(): List<ProxyWithHistory>
}