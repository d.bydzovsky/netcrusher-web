package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.rolling

import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.TrafficConstant
import java.io.File

class RollingFile(private val file: File) {

    init {
        if (file.nameWithoutExtension.contains(".")) {
            throw IllegalStateException("Filename cannot contain a dot character.")
        }
    }

    fun incrementFilesAndGetRecent(): File {
        if (file.length() > TrafficConstant.threshold) {
            incrementSuffixes(file)
        }
        return file
    }

    fun collectFilesAsc(): Collection<File> {
        return mutableListOf<File>().apply {
            iterateAsc { file -> this.add(file) }
        }
    }

    /**
     * Does not call a func even once when none file exists.
     * The most recent is first
     */
    fun iterateAsc(func: (file: File) -> Unit) {
        if (!file.exists()) return
        (0 until TrafficConstant.fileCount).forEach { index ->
            when(index) {
                0 -> func(file)
                else -> func(file.toIndex(index))
            }
        }
    }

    private fun incrementSuffixes(file: File) {
        (TrafficConstant.fileCount downTo 0).forEach { index ->
            when (index) {
                TrafficConstant.fileCount -> file.delete() // this is the last file, which will be rolled out
                0 -> file.renameTo(file.toIndex(1)) // the first file will be renamed to file with index 1
                else -> {
                    val actualFile = file.toIndex(index)
                    // increment suffix of actual file
                    actualFile.renameTo(file.toIndex(index + 1))
                }
            }
        }
    }

    private fun File.toIndex(index: Int): File {
        val adjustedNameWithoutExtension = if (isFileRolled()) {
            nameWithoutExtension.substringBeforeLast(".")
        } else {
            nameWithoutExtension
        }
        return File(parentFile, "$adjustedNameWithoutExtension.$index.$extension")
    }

    private fun File.isFileRolled(): Boolean {
        return nameWithoutExtension.contains(".")
    }

}