package com.horcacorp.testing.netcrusherweb.proxy.api

interface PersistorSwitcher {

    fun enable()

    fun disable()
}