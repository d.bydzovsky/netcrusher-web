package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.transformer

import com.horcacorp.testing.netcrusherweb.event.api.EventBus
import com.horcacorp.testing.netcrusherweb.event.api.ProxyTrafficItemPassed
import com.horcacorp.testing.netcrusherweb.event.api.RichProxyTrafficItemPassed
import com.horcacorp.testing.netcrusherweb.proxy.api.EnrichedTrafficItem
import com.horcacorp.testing.netcrusherweb.proxy.api.ProxyTrafficEnricherTransformer
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.enricher.TrafficEnricherFactory

class ProxyTrafficEnrichTransformerImpl(
    enricherFactory: TrafficEnricherFactory,
    private val eventBus: EventBus
) : ProxyTrafficEnricherTransformer {
    private val enricher = enricherFactory.create()

    override fun initialize() {
        eventBus.subscribe(ProxyTrafficItemPassed::class, ::onItemArrived)
    }

    override fun destroy() {
        eventBus.unsubscribe(ProxyTrafficItemPassed::class, ::onItemArrived)
    }

    private suspend fun onItemArrived(proxyItem: ProxyTrafficItemPassed) {
        // todo d.bydzovsky Performance: Do not transform item when no listener is out there...
        // todo d.bydzovsky Does use of lazy delegate work here?
        val enriched: EnrichedTrafficItem by lazy {
            enricher.enrich(proxyItem.item)
        }
        eventBus.broadcast(RichProxyTrafficItemPassed(proxyItem.bindPort, proxyItem.direction, enriched))
    }
}