package com.horcacorp.testing.netcrusherweb.proxy.impl

import com.horcacorp.testing.netcrusherweb.event.api.*
import com.horcacorp.testing.netcrusherweb.persistance.api.ProxyConfigPersistor
import com.horcacorp.testing.netcrusherweb.persistance.api.ProxyHistoryPersistor
import com.horcacorp.testing.netcrusherweb.proxy.api.*
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.io.IOException
import java.net.ServerSocket

class ProxyServiceImpl(
    private val eventBus: EventBus,
    private val proxyConfigPersistor: ProxyConfigPersistor,
    private val proxyHistoryPersistor: ProxyHistoryPersistor,
    private val netcrusherFactory: NetcrusherFactory
) : ProxyService {

    private val proxiesLock = Mutex()

    private var proxies = runBlocking {
        proxyConfigPersistor.getProxies().mapToSet { it.modelToProxy() }
    }

    override suspend fun add(proxyModel: ProxyModel) {
        proxiesLock.withLock {
            checkPort(proxyModel.connection.bindPort)
            checkFreePort(proxyModel.connection.bindPort)
            checkPort(proxyModel.connection.connectPort)
            updateProxies(proxies.plus(proxyModel.modelToProxy()))
            eventBus.broadcast(ProxyAdded(proxyModel))
        }
    }

    override suspend fun open(bindPort: Int) {
        proxiesLock.withLock {
            findProxy(bindPort).apply {
                updateProxies(
                    proxies
                        .minus(this)
                        .plus(open())
                )
            }
            eventBus.broadcast(ProxyOpened(bindPort))
        }
    }

    override suspend fun openAll() {
        proxiesLock.withLock {
            updateProxies(proxies.mapToSet { toBeOpened ->
                toBeOpened.open()
                    .also { opened -> eventBus.broadcast(ProxyOpened(opened.bindPort)) }
            })
        }
    }

    override suspend fun close(bindPort: Int) {
        proxiesLock.withLock {
            findProxy(bindPort).apply {
                updateProxies(
                    proxies
                        .minus(this)
                        .plus(close())
                )
            }
            eventBus.broadcast(ProxyClosed(bindPort))
        }
    }

    override suspend fun closeAll() {
        proxiesLock.withLock {
            updateProxies(proxies.mapToSet { toBeClosed ->
                toBeClosed.close()
                    .also { closed -> eventBus.broadcast(ProxyClosed(closed.bindPort)) }
            })
        }
    }

    override suspend fun freeze(bindPort: Int) {
        proxiesLock.withLock {
            findProxy(bindPort).apply {
                updateProxies(
                    proxies
                        .minus(this)
                        .plus(freeze())
                )
            }
            eventBus.broadcast(ProxyFroze(bindPort))
        }
    }

    override suspend fun freezeAll() {
        proxiesLock.withLock {
            updateProxies(proxies.mapToSet { toBeFrozen ->
                toBeFrozen.freeze()
                    .also { frozen -> eventBus.broadcast(ProxyFroze(frozen.bindPort)) }
            })
        }
    }

    override suspend fun remove(bindPort: Int) {
        proxiesLock.withLock {
            findProxy(bindPort).apply {
                destroy()
                updateProxies(proxies.minus(this))
            }
            eventBus.broadcast(ProxyRemoved(bindPort))
        }
    }

    override suspend fun removeAll() {
        proxiesLock.withLock {
            proxies.forEach {
                it.destroy()
                eventBus.broadcast(ProxyRemoved(it.bindPort))
            }
            updateProxies(emptySet())
        }
    }

    override suspend fun editDescription(bindPort: Int, newDescription: String) {
        proxiesLock.withLock {
            findProxy(bindPort).apply {
                updateProxies(
                    proxies
                        .minus(this)
                        .plus(editDescription(newDescription)
                            .also { eventBus.broadcast(ProxyEdited(bindPort, it.model)) }
                        )
                )
            }
        }
    }

    override suspend fun editConnection(bindPort: Int, newConnection: ProxyConnection) {
        proxiesLock.withLock {
            findProxy(bindPort).apply {
                if (bindPort != newConnection.bindPort) {
                    checkPort(newConnection.bindPort)
                    checkFreePort(newConnection.bindPort)
                }
                if (newConnection.connectPort != model.connection.connectPort) {
                    checkPort(newConnection.connectPort)
                }
                destroy()
                updateProxies(
                    proxies
                        .minus(this)
                        .plus(Proxy(model.copy(connection = newConnection), netcrusherFactory)
                            .also { eventBus.broadcast(ProxyEdited(bindPort, it.model)) }
                        )
                )
            }
        }
    }

    override suspend fun editThrottling(bindPort: Int, newDownConfig: ThrottlingConfig, newUpConfig: ThrottlingConfig) {
        proxiesLock.withLock {
            findProxy(bindPort).apply {
                updateProxies(
                    proxies
                        .minus(this)
                        .plus(editThrottling(newDownConfig, newUpConfig)
                            .also { eventBus.broadcast(ProxyEdited(bindPort, it.model)) }
                        )
                )
            }
        }
    }

    override suspend fun turnOnCommunicationPersisting(bindPort: Int) {
        proxiesLock.withLock {
            findProxy(bindPort).apply {
                updateProxies(
                    proxies
                        .minus(this)
                        .plus(turnOnCommunicationPersisting()
                            .also { eventBus.broadcast(ProxyEdited(bindPort, it.model)) }
                        )
                )
            }
        }
    }

    override suspend fun turnOffCommunicationPersisting(bindPort: Int) {
        proxiesLock.withLock {
            findProxy(bindPort).apply {
                updateProxies(
                    proxies
                        .minus(this)
                        .plus(turnOffCommunicationPersisting()
                            .also { eventBus.broadcast(ProxyEdited(bindPort, it.model)) }
                        )
                )
            }
        }
    }

    override suspend fun getAll() = proxiesLock.withLock { proxies.map { it.model } }

    override suspend fun getHistory(bindPort: Int): List<ProxyHistory> {
        return proxiesLock.withLock { proxyHistoryPersistor.getHistory(bindPort) }
    }

    override suspend fun getAllWithHistory(): List<ProxyWithHistory> {
        return proxiesLock.withLock {
            proxies.map { ProxyWithHistory(it.model, proxyHistoryPersistor.getHistory(it.bindPort)) }
        }
    }

    private fun checkPort(port: Int) {
        require(port >= 1) { "Invalid port '$port'. Cannot be lower than 1." }
        require(port <= 65535) { "Invalid port '$port'. Cannot be greater than 65535." }

    }

    private fun checkFreePort(port: Int) {
        checkPort(port)
        require(proxies.none { it.bindPort == port }) { "Bind port '$port' already used." }

        try {
            ServerSocket(port).close()
        } catch (e: IOException) {
            throw IllegalArgumentException("Bind port '$port' already used.")
        }
    }

    private fun findProxy(bindPort: Int): Proxy {
        return proxies.firstOrNull { it.bindPort == bindPort }
            ?: throw ProxyNotFoundException(bindPort)
    }

    private fun ProxyModel.modelToProxy() = Proxy(this, netcrusherFactory)

    private fun <T, R> Iterable<T>.mapToSet(transform: (T) -> R): Set<R> {
        return mapTo(HashSet(), transform)
    }

    private suspend fun updateProxies(newProxies: Set<Proxy>) {
        proxies = newProxies
        proxyConfigPersistor.saveProxies(newProxies.map { it.model })
    }
}