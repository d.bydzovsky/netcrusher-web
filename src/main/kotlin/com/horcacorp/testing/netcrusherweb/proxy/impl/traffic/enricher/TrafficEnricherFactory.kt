package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.enricher

class TrafficEnricherFactory {
    fun create() = TrafficEnricher()
}