package com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.enricher

import com.horcacorp.testing.netcrusherweb.proxy.api.EnrichedTrafficItem
import com.horcacorp.testing.netcrusherweb.proxy.api.TrafficType

data class UnknownTrafficItem(val body: String): EnrichedTrafficItem{

    override val type: TrafficType = TrafficType.UNKNOWN
}