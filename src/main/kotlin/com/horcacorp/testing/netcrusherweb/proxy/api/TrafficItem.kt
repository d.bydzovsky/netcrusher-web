package com.horcacorp.testing.netcrusherweb.proxy.api

interface EnrichedTrafficItem {
    val type: TrafficType
}

enum class TrafficType {
    HTTP, HTTPS, SQL, UNKNOWN
}