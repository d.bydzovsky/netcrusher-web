package com.horcacorp.testing.netcrusherweb.proxy.api

data class ProxyModel(
    val description: String,
    val connection: ProxyConnection,
    val downloadThrottling: ThrottlingConfig,
    val uploadThrottling: ThrottlingConfig,
    val state: ProxyState,
    val persistCommunication: Boolean
)

data class ProxyConnection(
    val bindPort: Int,
    val connectHost: String,
    val connectPort: Int
)

enum class ProxyState {
    OPEN, CLOSED, FROZEN
}

data class ThrottlingConfig(
    val value: Long,
    val mode: ThrottlingMode
)

enum class ThrottlingMode {
    OFF, BYTE_RATE, DELAY
}

data class ProxyWithHistory(
    val proxy: ProxyModel,
    val history: List<ProxyHistory>
)