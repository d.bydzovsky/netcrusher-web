package com.horcacorp.testing.netcrusherweb.webserver

import com.horcacorp.testing.netcrusherweb.configuration.ConfigurationProvider
import com.horcacorp.testing.netcrusherweb.event.impl.EventBusImpl
import com.horcacorp.testing.netcrusherweb.persistance.impl.PersistFilenameFactoryImpl
import com.horcacorp.testing.netcrusherweb.persistance.impl.FileSystemProxyConfigPersistor
import com.horcacorp.testing.netcrusherweb.persistance.impl.FileSystemProxyHistoryPersistor
import com.horcacorp.testing.netcrusherweb.persistance.impl.ManagedFilesMutexPoolImpl
import com.horcacorp.testing.netcrusherweb.proxy.impl.NetcrusherFactory
import com.horcacorp.testing.netcrusherweb.proxy.impl.ProxyServiceImpl
import com.horcacorp.testing.netcrusherweb.proxy.impl.history.ProxyHistoryWriterImpl
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.enricher.TrafficEnricherFactory
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.reader.TrafficReaderFactory
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.transformer.ProxyTrafficEnrichTransformerImpl
import com.horcacorp.testing.netcrusherweb.proxy.impl.traffic.writer.TrafficPersistorImpl
import com.horcacorp.testing.netcrusherweb.webserver.ktor.KtorWebserver
import com.horcacorp.testing.netcrusherweb.webserver.ktor.rest.KtorRestComponent
import com.horcacorp.testing.netcrusherweb.webserver.ktor.websocket.KtorWebsocketComponent
import com.horcacorp.testing.netcrusherweb.webserver.ktor.websocket.ProxyWebsocketResource
import org.slf4j.LoggerFactory

class NetcrusherWebApplication {

    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)

        @JvmStatic
        fun main(args: Array<String>) {
            val started = System.currentTimeMillis()
            val config = ConfigurationProvider()
            val eventBus = EventBusImpl()
            val managedFilesMutexPool = ManagedFilesMutexPoolImpl()
            val filenameFactory = PersistFilenameFactoryImpl(config)
            val persistor = FileSystemProxyConfigPersistor(managedFilesMutexPool, filenameFactory)
            val trafficEnricherFactory = TrafficEnricherFactory()

            val proxyHistoryPersistor = FileSystemProxyHistoryPersistor(managedFilesMutexPool, filenameFactory)
            val proxyHistoryWriter = ProxyHistoryWriterImpl(eventBus, proxyHistoryPersistor)
            proxyHistoryWriter.initialize()

            val proxyTrafficEnrichTransformerImpl = ProxyTrafficEnrichTransformerImpl(trafficEnricherFactory, eventBus)
            proxyTrafficEnrichTransformerImpl.initialize()

            val trafficPersistor = TrafficPersistorImpl(managedFilesMutexPool, eventBus, filenameFactory)
            trafficPersistor.initialize()

            val trafficReaderFactory = TrafficReaderFactory(managedFilesMutexPool, trafficEnricherFactory)
            val netcrusherFactory = NetcrusherFactory(eventBus, trafficReaderFactory, filenameFactory)
            val proxyService = ProxyServiceImpl(eventBus, persistor, proxyHistoryPersistor, netcrusherFactory)
            val wsResource = ProxyWebsocketResource(eventBus)
            KtorWebserver(config, listOf(KtorRestComponent(proxyService), KtorWebsocketComponent(wsResource)))
            logger.info("Netcrusher webserver up and running in ${System.currentTimeMillis() - started} milliseconds.")
        }
    }

}