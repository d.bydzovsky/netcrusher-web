package com.horcacorp.testing.netcrusherweb.webserver.ktor

import com.horcacorp.testing.netcrusherweb.configuration.ConfigurationProvider
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.content.resource
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.slf4j.LoggerFactory
import java.time.Duration

class KtorWebserver(config: ConfigurationProvider, components: List<KtorComponent>) {

    private val logger = LoggerFactory.getLogger(javaClass)
    private val port = config.serverPort

    init {
        embeddedServer(Netty, port = port) {
            logger.info("Starting Ktor webserver on port '$port'.")
            components.forEach { it.addComponent(this) }
            routing {
                static {
                    resource("/", "index.html")
                    resource("*", "index.html")
                    resources("/")
                }
            }

            install(CORS) {
                anyHost()
                method(HttpMethod.Get)
                method(HttpMethod.Post)
                method(HttpMethod.Put)
                method(HttpMethod.Patch)
                method(HttpMethod.Delete)
                method(HttpMethod.Options)
                allowXHttpMethodOverride()
                header(HttpHeaders.AccessControlAllowHeaders)
                header(HttpHeaders.ContentType)
                header(HttpHeaders.AccessControlAllowOrigin)
                allowCredentials = true
                anyHost()
                maxAgeInSeconds = Duration.ofDays(1).toSeconds()

            }
        }.start()
    }
}