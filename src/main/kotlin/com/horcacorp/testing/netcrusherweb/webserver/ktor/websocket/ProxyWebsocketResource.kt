package com.horcacorp.testing.netcrusherweb.webserver.ktor.websocket

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.horcacorp.testing.netcrusherweb.event.api.*
import com.horcacorp.testing.netcrusherweb.webserver.ktor.dto.*
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.WebSocketSession
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class ProxyWebsocketResource(eventBus: EventBus) {

    private val sessionsLock = Mutex()
    private val sessions = mutableSetOf<WebSocketSession>()
    private val websocketDispatcher = CoroutineScope(Dispatchers.IO)

    init {
        eventBus.subscribe(ProxyAdded::class, ::onProxyAdd)
        eventBus.subscribe(ProxyOpened::class, ::onProxyOpen)
        eventBus.subscribe(ProxyClosed::class, ::onProxyClose)
        eventBus.subscribe(ProxyFroze::class, ::onProxyFreeze)
        eventBus.subscribe(ProxyRemoved::class, ::onProxyRemove)
        eventBus.subscribe(ProxyEdited::class, ::onProxyEdit)
        eventBus.subscribe(ProxySpeedReport::class, ::onProxySpeedReport)
    }

    suspend fun onOpen(session: WebSocketSession) {
        sessionsLock.withLock { sessions.add(session) }
    }

    suspend fun onExit(session: WebSocketSession) {
        sessionsLock.withLock { sessions.remove(session) }
    }

    private suspend fun broadcast(message: String) {
        sessionsLock.withLock {
            sessions.forEach { send(it, Frame.Text(message)) }
        }
    }

    private fun send(session: WebSocketSession, frame: Frame) {
        websocketDispatcher.launch { session.send(frame) }
    }

    private suspend fun onProxyAdd(event: ProxyAdded) {
        broadcast(toJsonDto(ServerToClientAction.ADD, event.proxyModel.toDto()))
    }

    private suspend fun onProxyOpen(event: ProxyOpened) {
        broadcast(toJsonDto(ServerToClientAction.OPEN, event.bindPort))
    }

    private suspend fun onProxyClose(event: ProxyClosed) {
        broadcast(toJsonDto(ServerToClientAction.CLOSE, event.bindPort))
    }

    private suspend fun onProxyFreeze(event: ProxyFroze) {
        broadcast(toJsonDto(ServerToClientAction.FREEZE, event.bindPort))
    }

    private suspend fun onProxyRemove(event: ProxyRemoved) {
        broadcast(toJsonDto(ServerToClientAction.REMOVE, event.bindPort))
    }

    private suspend fun onProxyEdit(event: ProxyEdited) {
        broadcast(
            toJsonDto(
                ServerToClientAction.EDIT,
                WebsocketProxyEditedDto(event.bindPort, event.newProxyModel.toDto())
            )
        )
    }

    private suspend fun onProxySpeedReport(event: ProxySpeedReport) {
        broadcast(
            toJsonDto(
                ServerToClientAction.SPEED_REPORT,
                WebsocketSpeedReportDto(event.bindPort, event.bytesDown, event.bytesUp)
            )
        )
    }

    private fun toJsonDto(action: ServerToClientAction, data: Any): String {
        val dataJson = jacksonObjectMapper().writeValueAsString(data)
        return jacksonObjectMapper().writeValueAsString(ServerToClientWebsocketDto(action, dataJson))
    }

}