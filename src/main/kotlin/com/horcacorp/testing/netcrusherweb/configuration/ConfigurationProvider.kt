package com.horcacorp.testing.netcrusherweb.configuration

class ConfigurationProvider {

    val serverPort = System.getProperty("server.port")?.toInt() ?: 12000
    val devEnv = System.getProperty("dev.env")?.toBoolean() ?: false

}