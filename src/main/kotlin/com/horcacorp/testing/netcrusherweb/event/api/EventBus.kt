package com.horcacorp.testing.netcrusherweb.event.api

import kotlin.reflect.KClass

interface Event

interface EventBus {

    fun <T : Event> subscribe(event: KClass<T>, action: suspend (T) -> Unit)
    fun <T : Event> unsubscribe(event: KClass<T>, action: suspend (T) -> Unit)
    fun broadcast(event: Event)
}